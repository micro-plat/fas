//go:build beanpay
// +build beanpay

package fas

import (
	bpay "gitee.com/micro-plat/fas/modules/beanpay"
	"gitee.com/micro-plat/fas/modules/const/enum"
	"github.com/micro-plat/beanpay/beanpay"
	"github.com/micro-plat/beanpay/beanpay/account"
	"github.com/micro-plat/hydra"
	"github.com/micro-plat/lib4go/types"
)

//GetDownChannelList 获取记账系统下游渠道列表
//SourceSystemID 系统来源编号
func GetDownChannelList(SourceSystemID int) (list *DownChannelList, listType string, err error) {
	return nil, enum.BpayChannelList, err
}

//GetUpChannelList 获取记账系统上游渠道列表
//SourceSystemID 系统来源编号
func GetUpChannelList(SourceSystemID int) (list *UpChannelList, listType string, err error) {
	return nil, enum.BpayChannelList, err
}

//GetSupUpManualRecord sup上游账户人工操作记录
//upChannelNo-->上游渠道号
//changeTime-->记录时间 yyyy-mm-dd hh24:mi:ss 范围：开始时间点为changeTime减去一天，结束时间点为changeTime
func GetSupUpManualRecord(upChannelNo, changeTime string) (r *[]SupUpManualList, err error) {
	return nil, nil
}

//GetSupDownManualRecord sup下游账户人工操作记录
//upChannelNo-->上游渠道号
//changeTime-->记录时间 yyyy-mm-dd hh24:mi:ss 范围：开始时间点为changeTime减去一天，结束时间点为changeTime
func GetSupDownManualRecord(downChannelNo, changeTime string) (r *[]SupDownManualList, err error) {
	return nil, nil
}

//SupChannelInfoSync sup渠道同步
func SupChannelInfoSync(info *SupChannelSync) (r *SupSyncResult, err error) {
	_, err = beanpay.GetAccount(hydra.G.GetPlatName(), types.DecodeString(info.ChannelType, "1",
		enum.AccountGroups.UpChannel, enum.AccountGroups.DownChannel)).
		CreateAccount(nil, info.ChannelNo, info.ChannelName)
	if err != nil {
		return
	}
	r = &SupSyncResult{
		Result: "success",
		Msg:    "操作成功",
	}
	return
}

// CreateAccount 创建账户
//i-->nil/db.IDBTrans/db.IDB
//ident-->系统标识,accountType-->账户类型,1下游渠道账户,2上游渠道账户,默认是上游渠道账户
// eid-->外部用户账户编号,name-->账户名称
func CreateAccount(i interface{}, ident, accountType, eid, name string) error {
	return bpay.CreateAccount(i, ident, accountType, eid, name)
}

// UpdateAccount 修改账户
//i-->nil/db.IDBTrans/db.IDB
//ident-->系统标识,group-->用户分组,eid-->外部用户账户编号,name-->账户名称
func UpdateAccount(i interface{}, ident, group, eid, name string) (*account.AccountResult, error) {
	return beanpay.GetAccount(ident, group).SetAccountName(i, eid, name)
}

// GetAccount 查询账户信息
//i-->nil/db.IDBTrans/db.IDB
//ident-->系统标识,group-->用户分组,eid-->外部用户账户编号
func GetAccount(i interface{}, ident, group, eid string) (*account.Account, error) {
	return beanpay.GetAccount(ident, group).GetAccount(i, eid)
}

// QueryAccount 查询账户信息列表
// ident-->系统标识,group-->用户分组,eid-->外部用户账户编号,name-->账户名称
// accountType-->账户类型,1下游渠道账户,2上游渠道账户,默认是上游渠道账户,pi-->第几页, ps-->每页多少条, status-->账户状态
func QueryAccount(i interface{}, input *QueryAccountListInfo) (r *account.AccountInfoList, err error) {
	accountTypes := "up"
	if input.AccountType == enum.AccountTypes.Down {
		accountTypes = "down"
	}
	return beanpay.GetAccount(input.Ident, input.Group).QueryAccount(
		i, input.EID, accountTypes, input.Name, input.Status, input.PageIndex, input.PageSize)
}

// QueryTradeRecords 查询交易记录
//ident-->系统标识,group-->用户分组,eid-->外部用户账户编号,name-->账户名称
// startTime-->开始时间, endTime-->结束时间,pi-->第几页, ps-->每页多少条
func QueryTradeRecords(i interface{}, input *QueryTradeRecordsInfo) (*account.RecordResults, error) {
	accountTypes := "up"
	if input.AccountType == enum.AccountTypes.Down {
		accountTypes = "down"
	}
	return beanpay.GetAccount(input.Ident, input.Group).QueryAccountRecords(
		i, accountTypes, input.AccountID, input.Name, input.Group, input.ChangeType, input.TradeType,
		input.EID, input.StartTime, input.EndTime, input.PageIndex, input.PageSize)
}
