// +build beanpay

package fas

import (
	"github.com/micro-plat/beanpay/beanpay"
	"github.com/micro-plat/beanpay/beanpay/account"
	"github.com/micro-plat/beanpay/beanpay/const/ttypes"
	bpay "gitee.com/micro-plat/fas/modules/beanpay"
	"gitee.com/micro-plat/fas/modules/const/conf"
)

//DownChannelPay 下游扣款
func DownChannelPay(params *DownPay) (r *DownPayResult, err error) {
	if !conf.KeepAccount {
		return &DownPayResult{Result: "success", Msg: "下游扣款成功"}, nil
	}

	downpay := &bpay.DownPay{
		Ident:                params.Ident,
		DownChannelNo:        params.DownChannelNo,
		TradeOrderNo:         params.TradeOrderNo,
		DownSellAmount:       params.DownSellAmount,
		DownCommissionAmount: params.DownCommissionAmount,
		DownServiceAmount:    params.DownServiceAmount,
		DownFeeAmount:        params.DownFeeAmount,
		Memo:                 params.Memo,
	}
	if err := bpay.DownChannelPay(downpay); err != nil {
		return nil, err
	}

	return &DownPayResult{Result: "success", Msg: "下游扣款成功"}, nil
}

//SupDownChannelPay Sup下游扣款
func SupDownChannelPay(params *SupDownPay) (r *DownPayResult, err error) {
	if !conf.KeepAccount {
		return &DownPayResult{Result: "success", Msg: "Sup下游扣款成功"}, nil
	}
	downPay := &bpay.DownPay{
		Ident:                params.Ident,
		DownChannelNo:        params.DownChannelNo,
		TradeOrderNo:         params.TradeOrderNo,
		DownSellAmount:       params.DownSellAmount,
		DownCommissionAmount: params.DownCommissionAmount,
		DownServiceAmount:    params.DownServiceAmount,
		DownFeeAmount:        params.DownFeeAmount,
		Memo:                 params.Memo,
	}
	if err := bpay.DownChannelPay(downPay); err != nil {
		return nil, err
	}
	return &DownPayResult{Result: "success", Msg: "Sup下游扣款成功"}, nil
}

//UpChannelPay 上游扣款
func UpChannelPay(params *UpPay) (r *UpPayResult, err error) {
	if !conf.KeepAccount {
		return &UpPayResult{Result: "success", Msg: "上游扣款成功"}, nil
	}
	upPay := &bpay.UpPay{
		Ident:              params.Ident,
		UpChannelNo:        params.UpChannelNo,
		TradeDeliveryNo:    params.TradeDeliveryNo,
		UpCostAmount:       params.UpCostAmount,
		UpCommissionAmount: params.UpCommissionAmount,
		UpServiceAmount:    params.UpServiceAmount,
		Memo:               params.Memo,
	}
	if err := bpay.UpChannelPay(upPay); err != nil {
		return nil, err
	}
	return &UpPayResult{Result: "success", Msg: "上游扣款成功"}, nil
}

//SupUpChannelPay Sup上游扣款
func SupUpChannelPay(params *SupUpPay) (r *UpPayResult, err error) {
	if !conf.KeepAccount {
		return &UpPayResult{Result: "success", Msg: "Sup上游扣款成功"}, nil
	}
	upPay := &bpay.UpPay{
		Ident:              params.Ident,
		UpChannelNo:        params.UpChannelNo,
		TradeDeliveryNo:    params.TradeDeliveryNo,
		UpCostAmount:       params.UpCostAmount,
		UpCommissionAmount: params.UpCommissionAmount,
		UpServiceAmount:    params.UpServiceAmount,
		Memo:               params.Memo,
	}
	if err := bpay.UpChannelPay(upPay); err != nil {
		return nil, err
	}
	return &UpPayResult{Result: "success", Msg: "Sup上游扣款成功"}, nil
}

//DownChannelRefund 下游渠道退款
func DownChannelRefund(params *DownRefund) (r *DownPayResult, err error) {
	if !conf.KeepAccount {
		return &DownPayResult{Result: "success", Msg: "下游渠道退款成功"}, nil
	}
	downRefund := &bpay.DownRefund{
		Ident:                      params.Ident,
		DownChannelNo:              params.DownChannelNo,
		TradeOrderNo:               params.TradeOrderNo,
		TradeRefundNo:              params.TradeRefundNo,
		DownRefundAmount:           params.DownRefundAmount,
		DownRefundCommissionAmount: params.DownRefundCommissionAmount,
		DownRefundServiceAmount:    params.DownRefundServiceAmount,
		DownRefundFeeAmount:        params.DownRefundFeeAmount,
		Memo:                       params.Memo,
	}

	if err := bpay.DownChannelRefund(downRefund); err != nil {
		return nil, err
	}
	return &DownPayResult{Result: "success", Msg: "下游渠道退款成功"}, nil
}

//SupDownChannelRefund sup下游渠道退款
func SupDownChannelRefund(params *SupDownRefund) (r *DownPayResult, err error) {
	if !conf.KeepAccount {
		return &DownPayResult{Result: "success", Msg: "sup下游渠道退款成功"}, nil
	}
	downRefund := &bpay.DownRefund{
		Ident:                      params.Ident,
		DownChannelNo:              params.DownChannelNo,
		TradeOrderNo:               params.TradeOrderNo,
		TradeRefundNo:              params.TradeRefundNo,
		DownRefundAmount:           params.DownRefundAmount,
		DownRefundCommissionAmount: params.DownRefundCommissionAmount,
		DownRefundServiceAmount:    params.DownRefundServiceAmount,
		DownRefundFeeAmount:        params.DownRefundFeeAmount,
		Memo:                       params.Memo,
	}

	if err := bpay.DownChannelRefund(downRefund); err != nil {
		return nil, err
	}
	return &DownPayResult{Result: "success", Msg: "sup下游渠道退款成功"}, nil
}

//UpChannelRefund 上游渠道退款
func UpChannelRefund(params *UpRefund) (r *DownPayResult, err error) {
	if !conf.KeepAccount {
		return &DownPayResult{Result: "success", Msg: "上游渠道退款成功"}, nil
	}
	upRefund := &bpay.UpRefund{
		Ident:                    params.Ident,
		UpChannelNo:              params.UpChannelNo,
		TradeDeliveryNo:          params.TradeDeliveryNo,
		TradeRefundNo:            params.TradeRefundNo,
		UpRefundAmount:           params.UpRefundAmount,
		UpRefundCommissionAmount: params.UpRefundCommissionAmount,
		UpRefundServiceAmount:    params.UpRefundServiceAmount,
		Memo:                     params.Memo,
	}

	if err := bpay.UpChannelRefund(upRefund); err != nil {
		return nil, err
	}
	return &DownPayResult{Result: "success", Msg: "上游渠道退款成功"}, nil
}

//SupUpChannelRefund sup上游渠道退款
func SupUpChannelRefund(params *SupUpRefund) (r *DownPayResult, err error) {
	if !conf.KeepAccount {
		return &DownPayResult{Result: "success", Msg: "sup上游渠道退款成功"}, nil
	}
	upRefund := &bpay.UpRefund{
		Ident:                    params.Ident,
		UpChannelNo:              params.UpChannelNo,
		TradeDeliveryNo:          params.TradeDeliveryNo,
		TradeRefundNo:            params.TradeRefundNo,
		UpRefundAmount:           params.UpRefundAmount,
		UpRefundCommissionAmount: params.UpRefundCommissionAmount,
		UpRefundServiceAmount:    params.UpRefundServiceAmount,
		Memo:                     params.Memo,
	}

	if err := bpay.UpChannelRefund(upRefund); err != nil {
		return nil, err
	}
	return &DownPayResult{Result: "success", Msg: "sup上游渠道退款成功"}, nil
}

//GetSupZeroBalance 获取记账系统零点余额
//channelNo 渠道编号 rptDate yyyy-mm-dd
func GetSupZeroBalance(channelNo, rptDate string) (list *ZeroBalance, err error) {
	return nil, err
}

// AddAmount 加款
//i-->nil/db.IDBTrans/db.IDB
func AddAmount(i interface{}, ident, group, eid, tradeNo, memo string, amount float64) (*account.RecordResult, error) {
	return beanpay.GetAccount(ident, group).AddAmount(i, eid, tradeNo, amount, memo)
}

// DrawingAmount 提款
//i-->nil/db.IDBTrans/db.IDB
func DrawingAmount(i interface{}, ident, group, eid, tradeNo, memo string, amount float64) (*account.RecordResult, error) {
	return beanpay.GetAccount(ident, group).DrawingAmount(i, eid, tradeNo, amount, memo)
}

// ReverseAddAmount 红冲加款
//i-->nil/db.IDBTrans/db.IDB
func ReverseAddAmount(i interface{}, ident, group, eid, tradeNo, extNo, memo string) (*account.RecordResult, error) {
	return beanpay.GetAccount(ident, group).ReverseAddAmount(i, eid, tradeNo, extNo, memo)
}

// ReverseDrawingAmount 红冲提款
//i-->nil/db.IDBTrans/db.IDB
func ReverseDrawingAmount(i interface{}, ident, group, eid, tradeNo, extNo, memo string) (*account.RecordResult, error) {
	return beanpay.GetAccount(ident, group).ReverseDrawingAmount(i, eid, tradeNo, extNo, memo)
}

// TradeFlatAmount 交易平账
//i-->nil/db.IDBTrans/db.IDB
func TradeFlatAmount(i interface{}, ident, group, eid, tradeNo, memo string, tradeType ttypes.TradeType, amount float64) (*account.RecordResult, error) {
	return beanpay.GetAccount(ident, group).TradeFlatAmount(i, eid, tradeNo, tradeType, amount, memo)
}

// BalanceFlatAmount 余额平账
//i-->nil/db.IDBTrans/db.IDB
func BalanceFlatAmount(i interface{}, ident, group, eid, tradeNo, memo string, tradeType ttypes.TradeType, amount float64) (*account.RecordResult, error) {
	return beanpay.GetAccount(ident, group).BalanceFlatAmount(i, eid, tradeNo, tradeType, amount, memo)
}

// SetCreditAmount 设置授信金额
//i-->nil/db.IDBTrans/db.IDB
func SetCreditAmount(i interface{}, ident, group, eid string, credit float64) (*account.AccountResult, error) {
	return beanpay.GetAccount(ident, group).SetCreditAmount(i, eid, credit)
}
