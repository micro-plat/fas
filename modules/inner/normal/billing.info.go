package normal

type DownPay struct {
	OrderSource       int     `json:"order_source" m2s:"order_source" valid:"required"`                      //来源系统编号
	DownChannelNo     string  `json:"down_channel_no" m2s:"down_channel_no" valid:"required"`                //
	TradeOrderNo      string  `json:"trade_order_no" m2s:"trade_order_no" valid:"required"`                  //外部系统交易订单号
	ExtOrderNo        string  `json:"ext_order_no" m2s:"ext_order_no" valid:"required"`                      //下游商户的订单号
	RechargeAccountNo string  `json:"recharge_account_no" m2s:"recharge_account_no" valid:"required"`        //
	BusinessType      int     `json:"business_type" m2s:"business_type" valid:"required"`                    //
	CarrierNo         string  `json:"carrier_no" m2s:"carrier_no" valid:"required"`                          //
	ProvinceNo        string  `json:"province_no" m2s:"province_no" valid:"required"`                        //
	CityNo            string  `json:"city_no" m2s:"city_no" valid:"required"`                                //
	TotalFace         int     `json:"total_face" m2s:"total_face" valid:"range(0|999999999999999999)"`       //面值
	RechargeUnit      int     `json:"recharge_unit" m2s:"recharge_unit" valid:"range(0|999999999999999999)"` //充值量单位（多少M）
	OrderTime         string  `json:"order_time" m2s:"order_time" valid:"required"`                          //
	OrderAmount       float64 `json:"order_amount" m2s:"order_amount"`                                       //
	ServicePay        float64 `json:"service_pay" m2s:"service_pay"`                                         //手续费
	Memo              string  `json:"memo" m2s:"memo" valid:"required"`                                      //
}

type DownRefund struct {
	OrderSource   int     `json:"order_source" m2s:"order_source" valid:"required"`                        //来源系统编号
	DownChannelNo string  `json:"down_channel_no" m2s:"down_channel_no" valid:"required"`                  //
	TradeOrderNo  string  `json:"trade_order_no" m2s:"trade_order_no" valid:"required"`                    //外部系统交易订单号
	TradeRefundNo string  `json:"trade_refund_no" m2s:"trade_refund_no" valid:"required"`                  //外部系统交易订单号
	RefundUnit    int     `json:"refund_unit" m2s:"refund_unit" valid:"range(0|99999999999999999999)"`     //面值
	RefundFace    int     `json:"refund_face" m2s:"refund_face" valid:"range(0|99999999999999999999)"`     //面值
	RefundAmount  float64 `json:"refund_amount" m2s:"refund_amount" valid:"range(0|99999999999999999999)"` //充值量单位（多少M）
	RealRefund    float64 `json:"real_refund" m2s:"real_refund"`                                           //
	OrderDate     string  `json:"order_date" m2s:"order_date" valid:"required"`                            //
	ServiceFee    float64 `json:"service_fee" m2s:"service_fee"`                                           //手续费
	Memo          string  `json:"memo" m2s:"memo" valid:"required"`                                        //
}

type UpPay struct {
	OrderSource       int     `json:"order_source" m2s:"order_source" valid:"required"` //来源系统编号
	DownChannelNo     string  `json:"down_channel_no" m2s:"down_channel_no" valid:"required"`
	UpChannelNo       string  `json:"up_channel_no" m2s:"up_channel_no" valid:"required"`
	TradeOrderNo      string  `json:"trade_order_no" m2s:"trade_order_no" valid:"required"`
	TradeDeliveryNo   string  `json:"trade_delivery_no" m2s:"trade_delivery_no" valid:"required"`
	ExtOrderNo        string  `json:"ext_order_no" m2s:"ext_order_no" valid:"required"`
	RechargeAccountNo string  `json:"recharge_account_no" m2s:"recharge_account_no" valid:"required"`
	BillType          int     `json:"bill_type" m2s:"bill_type" valid:"required"`
	BusinessType      int     `json:"business_type" m2s:"business_type" valid:"required"`
	CarrierNo         string  `json:"carrier_no" m2s:"carrier_no" valid:"required"`
	ProvinceNo        string  `json:"province_no" m2s:"province_no" valid:"required"`
	CityNo            string  `json:"city_no" m2s:"city_no" valid:"required"`
	DownOrderUnit     int     `json:"down_order_unit" m2s:"down_order_unit" valid:"range(0|999999999999999999)"`
	DownOrderFace     int     `json:"down_order_face" m2s:"down_order_face" valid:"range(0|999999999999999999)"`
	DownDrawUnit      int     `json:"down_draw_unit" m2s:"down_draw_unit" valid:"range(0|99999999999999999999)"`
	DownDrawFace      int     `json:"down_draw_face" m2s:"down_draw_face" valid:"range(0|99999999999999999999)"`
	DownDrawAmount    float64 `json:"down_draw_amount" m2s:"down_draw_amount"`
	DownRealAmount    float64 `json:"down_real_amount" m2s:"down_real_amount"`
	UpDrawUnit        int     `json:"up_draw_unit" m2s:"up_draw_unit" valid:"range(0|99999999999999999999)"`
	UpDrawFace        int     `json:"up_draw_face" m2s:"up_draw_face" valid:"range(0|99999999999999999999)"`
	UpDrawAmount      float64 `json:"up_draw_amount" m2s:"up_draw_amount" `
	UpRealAmount      float64 `json:"up_real_amount" m2s:"up_real_amount" `
	OrderTime         string  `json:"order_time" m2s:"order_time" valid:"required"`
	ServiceFee        float64 `json:"service_fee" m2s:"service_fee" ` //手续费
	Memo              string  `json:"memo" m2s:"memo" valid:"required"`
}

type UpRefund struct {
	OrderSource        int     `json:"order_source" m2s:"order_source" valid:"required"`           //来源系统编号
	DownChannelNo      string  `json:"down_channel_no" m2s:"down_channel_no" valid:"required"`     //
	UpChannelNo        string  `json:"up_channel_no" m2s:"up_channel_no" valid:"required"`         //
	TradeOrderNo       string  `json:"trade_order_no" m2s:"trade_order_no" valid:"required"`       //
	TradeDeliveryNo    string  `json:"trade_delivery_no" m2s:"trade_delivery_no" valid:"required"` //
	TradeRefundNo      string  `json:"trade_refund_no" m2s:"trade_refund_no" valid:"required"`     //
	BillType           int     `json:"bill_type" m2s:"bill_type" valid:"required"`
	BusinessType       int     `json:"business_type" m2s:"business_type" valid:"required"`
	CarrierNo          string  `json:"carrier_no" m2s:"carrier_no" valid:"required"`
	ProvinceNo         string  `json:"province_no" m2s:"province_no" valid:"required"`
	UpRefundUnit       int     `json:"up_refund_unit" m2s:"up_refund_unit" valid:"range(0|99999999999999999999)"`
	UpRefundFace       int     `json:"up_refund_face" m2s:"up_refund_face" valid:"range(0|99999999999999999999)"`
	UpRefundAmount     float64 `json:"up_refund_amount" m2s:"up_refund_amount " `
	UpRefundRealAmount float64 `json:"up_refund_real_amount" m2s:"up_refund_real_amount"`
	OrderTime          string  `json:"order_time" m2s:"order_time" valid:"required"`
	ServiceFee         float64 `json:"service_fee" m2s:"service_fee"`    //手续费
	Memo               string  `json:"memo" m2s:"memo" valid:"required"` //
}
