package sup

type DownPay struct {
	DownChannelNo string  `json:"down_channel_no" m2s:"down_channel_no" valid:"required"`                //下游渠道编号
	TradeOrderNo  string  `json:"trade_order_no" m2s:"trade_order_no" valid:"required"`                  //下游交易订单号
	BusinessType  int     `json:"business_type" m2s:"business_type" valid:"required"`                    //业务类型，记账系统的
	CarrierNo     string  `json:"carrier_no" m2s:"carrier_no" valid:"required"`                          //运营商
	ProvinceNo    string  `json:"province_no" m2s:"province_no" valid:"required"`                        //省份
	CityNo        string  `json:"city_no" m2s:"city_no" valid:"required"`                                //城市
	OrderUnit     int     `json:"order_unit" m2s:"order_unit" valid:"range(0|99999999999999999999)"`     //订单规格
	OrderFace     int     `json:"order_face" m2s:"order_face" valid:"range(0|99999999999999999999)"`     //订单金额面值
	OrderAmount   float64 `json:"order_amount" m2s:"order_amount" valid:"range(0|99999999999999999999)"` //订单扣款金额
	OrderTime     string  `json:"order_time" m2s:"order_time" valid:"required"`                          //生成系统订单生成时间，格式：yyyy-mm-dd hh24:mi:ss
	ChangeTime    string  `json:"change_time" m2s:"change_time" valid:"required"`                        //生成系统下游扣款时间，格式：yyyy-mm-dd hh24:mi:ss
	Memo          string  `json:"memo" m2s:"memo" valid:"required"`                                      //扣款备注信息
}

type DownRefund struct {
	DownChannelNo string  `json:"down_channel_no" m2s:"down_channel_no" valid:"required"`                  //下游渠道编号
	TradeOderNo   string  `json:"trade_order_no" m2s:"trade_order_no" valid:"required"`                    //下游交易订单号
	TradeRefundNo string  `json:"trade_refund_no" m2s:"trade_refund_no" valid:"required"`                  //下游退款编号
	RefundFace    int     `json:"refund_face" m2s:"refund_face" valid:"range(0|99999999999999999999)"`     //退款金额面值
	RefundAmount  float64 `json:"refund_amount" m2s:"refund_amount" valid:"range(0|99999999999999999999)"` //退款金额
	OrderTime     string  `json:"order_time" m2s:"order_time" valid:"required"`                            //生成系统订单生成时间，格式：yyyy-mm-dd hh24:mi:ss
	ChangeTime    string  `json:"change_time" m2s:"change_time" valid:"required"`                          //生成系统下游退款时间，格式：yyyy-mm-dd hh24:mi:ss
	Memo          string  `json:"memo" m2s:"memo" valid:"required"`                                        //扣款备注信息
}

type SupUpPay struct {
	UpChannelNo     string  `json:"up_channel_no" m2s:"up_channel_no" valid:"required"`                        //上游渠道编号
	DownChannelNo   string  `json:"down_channel_no" m2s:"down_channel_no" valid:"required"`                    //下游渠道编号
	TradeOrderNo    string  `json:"trade_order_no" m2s:"trade_order_no" valid:"required"`                      //交易订单号
	TradeDeliveryNo string  `json:"trade_delivery_no" m2s:"trade_delivery_no" valid:"required"`                //交易发货编号
	BillType        string  `json:"bill_type" m2s:"bill_type" valid:"in(1|2)"`                                 //开票信息，前后项，1前向，不开票，2后项，需要开票
	BusinessType    int     `json:"business_type" m2s:"business_type" valid:"required"`                        //业务类型，记账系统的
	CarrierNo       string  `json:"carrier_no" m2s:"carrier_no" valid:"required"`                              //运营商
	ProvinceNo      string  `json:"province_no" m2s:"province_no" valid:"required"`                            //省份
	CityNo          string  `json:"city_no" m2s:"city_no" valid:"required"`                                    //城市编号
	OrderUnit       int     `json:"order_unit" m2s:"order_unit" valid:"range(0|99999999999999999999)"`         //订单规格
	OrderFace       int     `json:"order_face" m2s:"order_face" valid:"range(0|99999999999999999999)"`         //订单金额面值
	OrderAmount     float64 `json:"order_amount" m2s:"order_amount" valid:"range(0|99999999999999999999)"`     //订单金额
	UpDrawCount     int     `json:"up_draw_count" m2s:"up_draw_count" valid:"range(0|99999999999999999999)"`   //上游扣款消耗库存卡张数
	UpDrawUnit      int     `json:"up_draw_unit" m2s:"up_draw_unit" valid:"range(0|99999999999999999999)"`     //上游扣款规格
	UpDrawFace      int     `json:"up_draw_face" m2s:"up_draw_face" valid:"range(0|99999999999999999999)"`     //上游扣款金额面值
	UpDrawAmount    float64 `json:"up_draw_amount" m2s:"up_draw_amount" valid:"range(0|99999999999999999999)"` //上游扣款金额
	OrderTime       string  `json:"order_time" m2s:"order_time" valid:"required"`                              //订单创建时间
	ChangeTime      string  `json:"change_time" m2s:"change_time" valid:"required"`                            //上游扣款发生时间SupDownOrderPay
	Memo            string  `json:"memo" m2s:"memo" valid:"required"`                                          //备注信息
}

type UpRefund struct {
	UpChannelNo     string  `json:"up_channel_no" m2s:"up_channel_no" valid:"required"`                            //上游渠道编号
	DownChannelNo   string  `json:"down_channel_no" m2s:"down_channel_no" valid:"required"`                        //下游渠道编号
	TradeOrderNo    string  `json:"trade_order_no" m2s:"trade_order_no" valid:"required"`                          //交易订单号
	TradeDeliveryNo string  `json:"trade_delivery_no" m2s:"trade_delivery_no" valid:"required"`                    //订单发货编号
	TradeRefundNo   string  `json:"trade_refund_no" m2s:"trade_refund_no" valid:"required"`                        //订单上游退款编号
	BillType        string  `json:"bill_type" m2s:"bill_type" valid:"range(1|2)"`                                  //开票信息，前后项，1前向，不开票，2后项，需要开票
	BusinessType    int     `json:"business_type" m2s:"business_type" valid:"required"`                            //业务类型，记账系统的
	UpRefundCount   int     `json:"up_refund_count" m2s:"up_refund_count" valid:"range(0|99999999999999999999)"`   //上游退款卡张数
	UpRefundUnit    int     `json:"up_refund_unit" m2s:"up_refund_unit" valid:"range(0|99999999999999999999)"`     //上游退款规格
	UpRefundFace    int     `json:"up_refund_face" m2s:"up_refund_face" valid:"range(0|99999999999999999999)"`     //上游退款金额面值
	UpRefundAmount  float64 `json:"up_refund_amount" m2s:"up_refund_amount" valid:"range(0|99999999999999999999)"` //上游退款金额
	OrderTime       string  `json:"order_time" m2s:"order_time" valid:"required"`                                  //订单创建时间
	ChangeTime      string  `json:"change_time" m2s:"change_time" valid:"required"`                                //上游退款发生时间
	Memo            string  `json:"memo" m2s:"memo" valid:"required"`                                              //备注信息
}

type ChannelInfoSync struct {
	ChannelNo      string `json:"channel_no" m2s:"channel_no" valid:"required"`          //渠道编号
	ChannelName    string `json:"channel_name" m2s:"channel_name" valid:"required"`      //渠道名称
	CompanyID      string `json:"company_id" m2s:"company_id"`                           //渠道所属公司编号
	SystemID       string `json:"system_id" m2s:"system_id"`                             //渠道所属系统编号
	StatisticsType string `json:"statistics_type" m2s:"statistics_type" valid:"in(0|4)"` //上游渠道的财务分类，上游是4，下游是0
	ChannelType    string `json:"channel_type" m2s:"channel_type" valid:"in(1|2)"`       //渠道类型，1上游，2下游
	Status         string `json:"status" m2s:"status" valid:"in(0|1)"`                   //渠道状态，0启用，1禁用
	OperateUser    string `json:"operate_user" m2s:"operate_user" valid:"required"`      //操作人
}
