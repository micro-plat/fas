package conf

//SupSetting x
var SupSetting = &SupConf{}

type SupConf struct {
	CompanyID string `json:"company_id"`
	SystemID  string `json:"system_id"`
}

func GetSupCompanyID() string {
	return SupSetting.CompanyID
}

func GetSupSystemID() string {
	return SupSetting.SystemID
}
