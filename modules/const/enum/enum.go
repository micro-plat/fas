package enum

// AccountTypes 账户类型
var AccountTypes = &struct {
	Up   string
	Down string
}{"2", "1"}

// AccountGroups 账户分组
var AccountGroups = &struct {
	DownChannel    string
	DownCommission string
	DownService    string
	UpChannel      string
	UpCommission   string
	UpService      string
}{"down_channel", "down_commission", "down_service", "up_channel", "up_commission", "up_service"}
