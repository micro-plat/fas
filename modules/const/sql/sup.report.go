package sql

const GetSupReportZeroBalance = `
select t.channel_no, 
       to_char(t.on_way_balance + t.physical_card_balance + t.electronic_card_balance) zero_balance,
       t.channel_type,
       to_char(t.zero_date,'yyyy-mm-dd') zero_date
  from sup_report_zero_balance t
 where t.channel_no  =@channel_no
  and t.zero_date = to_date(@rpt_date,'yyyy-mm-dd')
`
