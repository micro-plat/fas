package sql

//--11加款，12加款红冲，13提款，14提款红冲
const SupUpManualRecord = `
select 
        t.record_id,
        t.bank_record_id,
        t.bank_card_no,
        t.change_type,
        t.adjust_record_id,
        to_char(t.way_change_amount+t.physical_change_amount+t.electronic_change_amount) way_change_amount,
        to_char(t.way_change_face+t.physical_change_face+t.electronic_change_face) way_change_face,      
        to_char(t.change_time,'yyyy-mm-dd hh24:mi:ss') change_time,
        t.created_by,
        t.memo
from sup_trade_up_manual t
where
        t.up_channel_no = @up_channel_no
        and  t.change_type in (11, 12, 13, 14,60)
        and t.change_time >= to_date(@change_time,'yyyy-mm-dd hh24:mi:ss') -1
        and t.change_time < to_date(@change_time,'yyyy-mm-dd hh24:mi:ss')
order by t.record_id asc
`

//--11加款，12加款红冲，13提款，14提款红冲
const SupDownManualRecord = `
select 
        t.record_id,
        t.bank_record_id,
        t.bank_card_no,
        t.change_type,
        to_char(t.change_amount) change_amount,
        t.adjust_record_id,
        to_char(t.change_time,'yyyy-mm-dd hh24:mi:ss') change_time,
        t.created_by,
        t.memo
from sup_trade_down_manual t
where
        t.DOWN_CHANNEL_NO = @down_channel_no
        and  t.change_type in (11, 12, 13, 14,60)
        and t.change_time >= to_date(@change_time,'yyyy-mm-dd hh24:mi:ss') -1
        and t.change_time < to_date(@change_time,'yyyy-mm-dd hh24:mi:ss')
order by t.record_id asc
`
