package sql

const QueryFdDownChannelInfo = `
select
t.channel_no,
c.channel_name,
c.company_id,
c.status,
c.channel_company,
c.channel_full_name
from
fd_base_down_system_map t
left join fd_base_down_channel c on c.channel_no=t.channel_no 
where
t.source_system_id=@source_system_id
`

const QueryFdUpChannelInfo = `
select
t.channel_no,
c.channel_name,
c.company_id,
c.status,
c.channel_company,
c.channel_full_name
from
fd_base_up_system_map t
left join fd_base_up_channel c on c.channel_no=t.channel_no 
where
t.source_system_id=@source_system_id
`
