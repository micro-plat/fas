package sql

/*
v_down_channel_no varchar2, ---下游渠道编号
v_trade_order_no  varchar2, ---下游交易订单号
v_business_type   number, ---业务类型
v_carrier_no      varchar2, ---运营商
v_province_no     varchar2, ---省份
v_city_no         varchar2, ---城市
v_order_unit      number, ---订单规格
v_order_face      number, ---订单金额面值
v_order_amount    number, ---订单扣款金额
v_order_time      varchar2, ---生成系统订单生成时间，格式：yyyy-mm-dd hh24:mi:ss
v_change_time     varchar2, ---生成系统下游扣款时间，格式：yyyy-mm-dd hh24:mi:ss
v_memo            varchar2, ---扣款备注信息
v_result          out varchar2,
v_msg             out varchar2
*/
const SupDownOrderPay = `
sup_p_trade_down_order_pay(
	@down_channel_no,
	@trade_order_no,  
	@business_type, 
	@carrier_no,     
	@province_no,  
	@city_no,        
	@order_unit,      
	@order_face,     
	@order_amount,   
	@order_time,      
	@change_time,     
	@memo,      
	:result,
	:msg
)
`

/*
v_up_channel_no     varchar2, ---上游渠道编号
v_down_channel_no   varchar2, ---下游渠道编号
v_trade_order_no    varchar2, ---交易订单号
v_trade_delivery_no varchar2, ---交易发货编号
v_bill_type         number, ---开票信息，前后项，1前向，不开票，2后项，需要开票
v_business_type     number, ---业务类型
v_carrier_no        varchar2, ---运营商
v_province_no       varchar2, ---省份
v_city_no           varchar2, ---城市编号
v_order_unit        number, ---订单规格
v_order_face        number, ---订单金额面值
v_order_Amount      number, ---订单金额
v_up_draw_count     number, ---上游扣款消耗库存卡张数
v_up_draw_unit      number, ---上游扣款规格
v_up_draw_face      number, ---上游扣款金额面值
v_up_draw_amount    number, ---上游扣款金额
v_order_time        varchar2, ---订单创建时间
v_change_time       varchar2, ---上游扣款发生时间SupDownOrderPay
v_memo              varchar2, ---备注信息
v_result            out varchar2,
v_msg               out varchar2
*/
const SupUpOrderPay = `
sup_p_trade_up_order_pay(
	@up_channel_no,
	@down_channel_no,
	@trade_order_no,
	@trade_delivery_no,
	@bill_type,
	@business_type,
	@carrier_no,
	@province_no,
	@city_no,
	@order_unit,
	@order_face,
	@order_amount,
	@up_draw_count,
	@up_draw_unit,
	@up_draw_face,
	@up_draw_amount,
	@order_time,
	@change_time,
	@memo,
	:result,
	:msg
)
`
