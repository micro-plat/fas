package sql

// v_channel_no      varchar2, ---渠道编号
// v_channel_name    varchar2, ---渠道名称
// v_company_id      number, ---渠道所属公司编号
// v_system_id       number, ---渠道所属系统编号
// v_statistics_type number, ---上游渠道的财务分类，下游为0
// v_channel_type    number, ---渠道类型，1上游，2下游
// v_status          number, ---渠道状态，0启用，1禁用
// v_operate_user    varchar2, ---操作人
// v_operate_time    varchar2, ---操作时间,格式 yyyy-mm-dd hh24:mi:ss
// v_result          out varchar2,
// v_msg             out varchar2
//sup_p_sync_channel_info
const SupChannelInfoSync = `
sup_p_sync_channel_info(
	@channel_no,
	@channel_name,
	@company_id,
	@system_id,
	@statistics_type,
	@channel_type,
	@status,
	@operate_user,
	to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'),
	:result,
	:msg
)
`
