package sql

// v_order_source        in number, ---来源系统编号
// v_down_channel_no     in varchar2,
// v_trade_order_no      in varchar2, ---外部系统交易订单号
// v_ext_order_no        in varchar2, ---下游商户的订单号
// v_recharge_account_no in varchar2,
// v_business_type       in number,
// v_carrier_no          in varchar2,
// v_province_no         in varchar2,
// v_city_no             in varchar2,
// v_total_face          in number, --面值
// v_recharge_unit       in number, --充值量单位（多少M）
// v_order_time          in varchar2,
// v_order_amount        in number,
// v_service_pay         in number, --手续费
// v_memo                in varchar2,
// v_result              out varchar2,
// v_msg                 out varchar2
const FDDownOrderFeePay = `
fd_p_trade_down_order_fee_pay(
@order_source,
@down_channel_no,
@trade_order_no,
@ext_order_no,
@recharge_account_no,
@business_type,
@carrier_no,
@province_no,
@city_no,
@total_face,
@recharge_unit,
@order_time,
@order_amount,
@service_pay,
@memo,
:result,
:msg
)
`

//v_order_source        in number,
// v_down_channel_no     in varchar2,
// v_up_channel_no       in varchar2,
// v_trade_order_no      in varchar2,
// v_trade_delivery_no   in varchar2,
// v_ext_order_no        in varchar2,
// v_recharge_account_no in varchar2,
// v_bill_type           in number,
// v_business_type       in number,
// v_carrier_no          in varchar2,
// v_province_no         in varchar2,
// v_city_no             in varchar2,
// v_down_order_unit     in number,
// v_down_order_face     in number,
// v_down_draw_unit      in number,
// v_down_draw_face      in number,
// v_down_draw_amount    in number,
// v_down_real_amount    in number,
// v_up_draw_unit        in number,
// v_up_draw_face        in number,
// v_up_draw_amount      in number,
// v_up_real_amount      in number,
// v_order_time          in varchar2,
// v_service_fee         in number, --手续费
// v_memo                in varchar2,
// v_result              out varchar2
const FDUpOrderFeePay = `
fd_p_trade_up_order_pay(
	@order_source,
	@down_channel_no,
	@up_channel_no,
	@trade_order_no,
	@trade_delivery_no,
	@ext_order_no,
	@recharge_account_no,
	@bill_type,
	@business_type,
	@carrier_no,
	@province_no,
	@city_no,
	@down_order_unit,
	@down_order_face,
	@down_draw_unit,
	@down_draw_face,
	@down_draw_amount,
	@down_real_amount,
	@up_draw_unit,
	@up_draw_face,
	@up_draw_amount,
	@up_real_amount,
	@order_time,
	@service_fee,
	@memo,
	:result
)
`
