package sql

// v_order_source    in number,
// v_down_channel_no in varchar2,
// v_trade_order_no  in varchar2,
// v_trade_refund_no in varchar2,
// v_refund_unit     in number,
// v_refund_face     in number,
// v_refund_amount   in number,
// v_real_refund     in number,
// v_order_date      in varchar2,
// v_service_fee     in number,
// v_memo            in varchar2,
// v_result          out varchar2

const FDDownOrderFeeRefund = `
fd_p_trade_down_order_fee_ref(
@order_source,
@down_channel_no,
@trade_order_no,
@trade_refund_no,
@refund_unit,
@refund_face,
@refund_amount,
@real_refund,
@order_date,
@service_fee,
@memo,
:result
)
`

// v_order_source          in number,
// v_down_channel_no       in varchar2,
// v_up_channel_no         in varchar2,
// v_trade_order_no        in varchar2,
// v_trade_delivery_no     in varchar2,
// v_trade_refund_no       in varchar2,
// v_bill_type             in number,
// v_business_type         in number,
// v_carrier_no            in varchar2,
// v_province_no           in varchar2,
// v_up_refund_unit        in number,
// v_up_refund_face        in number,
// v_up_refund_amount      in number,
// v_up_refund_real_amount in number,
// v_order_time            in varchar2,
// v_service_fee           in number,
// v_memo                  in varchar2,
// v_result                out varchar2

const FDUpOrderFeeRefund = `
fd_p_trade_up_order_refund(
@order_source,
@down_channel_no,
@up_channel_no,
@trade_order_no,
@trade_delivery_no,
@trade_refund_no,
@bill_type,
@business_type,
@carrier_no,
@province_no,
@up_refund_unit,
@up_refund_face,
@up_refund_amount,
@up_refund_real_amount,
@order_time,
@service_fee,
@memo,
:result
)
`
