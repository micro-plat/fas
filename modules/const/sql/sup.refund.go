package sql

/*
v_down_channel_no varchar2, ---下游渠道编号
v_trade_order_no  varchar2, ---下游交易订单号
v_trade_refund_no varchar2, ---下游退款编号
v_refund_face     number, ---退款金额面值
v_refund_amount   number, ---退款金额
v_order_time      varchar2, ---生成系统订单生成时间，格式：yyyy-mm-dd hh24:mi:ss
v_change_time     varchar2, ---生成系统下游退款时间，格式：yyyy-mm-dd hh24:mi:ss
v_memo            varchar2, ---扣款备注信息
v_result          out varchar2,
v_msg             out varchar2 */
const SupDownOrderRefund = `
sup_p_trade_down_order_refund(
	@down_channel_no, 
	@trade_order_no, 
	@trade_refund_no, 
	@refund_face,
	@refund_amount,
	@order_time,
	@change_time,
	@memo,
	:result,
	:msg
)
`

/*
v_up_channel_no     varchar2, ---上游渠道编号
v_down_channel_no   varchar2, ---下游渠道编号
v_trade_order_no    varchar2, ---交易订单号
v_trade_delivery_no varchar2, ---订单发货编号
v_trade_refund_no   varchar2, ---订单上游退款编号
v_bill_type         number, ---开票信息，前后项，1前向，不开票，2后项，需要开票
v_business_type     number, ---业务类型
v_up_refund_count   number, ---上游退款卡张数
v_up_refund_unit    number, ---上游退款规格
v_up_refund_face    number, ---上游退款金额面值
v_up_refund_amount  number, ---上游退款金额
v_order_time        varchar2, ---订单创建时间
v_change_time       varchar2, ---上游退款发生时间
v_memo              varchar2, ---备注信息
v_result            out varchar2,
v_msg               out varchar2 */

const SupUpOrderRefund = `
sup_p_trade_up_order_refund(
	@up_channel_no,
	@down_channel_no,
	@trade_order_no,
	@trade_delivery_no,
	@trade_refund_no,
	@bill_type,
	@business_type,
	@up_refund_count,
	@up_refund_unit,
	@up_refund_face,
	@up_refund_amount,
	@order_time,
	@change_time,
	@memo,
	:result,
	:msg
)
`
