package util

import (
	"fmt"

	"github.com/micro-plat/lib4go/types"
)

// FloatDecimal float精度
func FloatDecimal(data types.XMap) (types.XMap, error) {
	for k, v := range data {
		switch v.(type) {
		case float32, float64:
			data.SetValue(k, fmt.Sprintf("%.5f", v))
		}
	}
	return data, nil
}
