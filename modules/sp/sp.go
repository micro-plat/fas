package sp

import (
	"database/sql"
	"fmt"

	"gitee.com/micro-plat/fas/modules/const/conf"
	"github.com/micro-plat/hydra"
	"github.com/micro-plat/lib4go/types"
)

func ExecuteSP(SP string, param types.XMap) (result, msg string, err error) {
	db, err := hydra.C.DB().GetDB(conf.DBName)
	if err != nil {
		return "", "", err
	}
	_, err = db.ExecuteSP(SP, param, sql.Named("result", sql.Out{Dest: &result}), sql.Named("msg", sql.Out{Dest: &msg}))
	if err != nil {
		return "", "", fmt.Errorf("执行记账系统%s存储过程失败:%+v,data:%+v", SP, err, param)
	}

	return result, msg, nil
}

func ExecuteSPWithResult(SP string, param types.XMap) (result string, err error) {
	db, err := hydra.C.DB().GetDB(conf.DBName)
	if err != nil {
		return "", err
	}
	_, err = db.ExecuteSP(SP, param, sql.Named("result", sql.Out{Dest: &result}))
	if err != nil {
		return "", fmt.Errorf("执行记账系统%s存储过程失败:%+v,data:%+v", SP, err, param)
	}

	return result, nil
}
