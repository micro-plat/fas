package sdkdb

import (
	"gitee.com/micro-plat/fas/modules/const/conf"
	"github.com/micro-plat/hydra"
	"github.com/micro-plat/lib4go/db"
)

func GetDBTrans(c interface{}) (bool, db.IDBTrans, error) {
	b, e, err := getDB(c)
	if err != nil {
		return false, nil, err
	}
	if b {
		return false, e.(db.IDBTrans), nil
	}
	t, err := e.(db.IDB).Begin()
	if err != nil {
		return false, nil, err
	}
	return true, t, nil
}

func getDB(c interface{}) (bool, db.IDBExecuter, error) {
	switch v := c.(type) {
	case db.IDB:
		return false, v, nil
	case db.IDBTrans:
		return true, v, nil
	default:
		c, err := hydra.C.DB().GetDB(conf.DBName)
		return false, c, err
	}
}
