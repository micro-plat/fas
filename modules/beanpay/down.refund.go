package beanpay

import (
	"gitee.com/micro-plat/fas/modules/const/enum"
	"gitee.com/micro-plat/fas/modules/sdkdb"
	"github.com/asaskevich/govalidator"
	"github.com/micro-plat/beanpay/beanpay"
	"github.com/micro-plat/lib4go/errs"
)

//DownChannelRefund 下游退款
func DownChannelRefund(param *DownRefund) error {

	// 验证参数
	if b, err := govalidator.ValidateStruct(param); !b {
		return errs.NewErrorf(406, "BPay:DownChannelRefund:govalidator.参数验证错误err:%+v,param:%+v", err, param)
	}

	// 获取数据操作对象
	_, trans, err := sdkdb.GetDBTrans(nil)
	if err != nil {
		return err
	}

	// 渠道账户退款
	bp := beanpay.GetAccount(param.Ident, enum.AccountGroups.DownChannel)
	if param.DownRefundAmount > 0 {
		_, err := bp.RefundAmount(trans, param.DownChannelNo, param.TradeRefundNo, param.TradeOrderNo, beanpay.TPTrade, param.DownRefundAmount, param.Memo)
		if err != nil {
			trans.Rollback()
			return err
		}
	}

	// 手续费退款
	if param.DownRefundFeeAmount > 0 {
		_, err := bp.RefundAmount(trans, param.DownChannelNo, param.TradeRefundNo, param.TradeOrderNo, beanpay.TPFree, -param.DownRefundFeeAmount, param.Memo)
		if err != nil {
			trans.Rollback()
			return err
		}
	}

	// 佣金退款
	if param.DownRefundCommissionAmount != 0 {
		_, err := beanpay.GetAccount(param.Ident, enum.AccountGroups.DownCommission).DrawingAmount(
			trans, param.DownChannelNo, param.TradeOrderNo, param.DownRefundCommissionAmount, param.Memo)
		if err != nil {
			trans.Rollback()
			return err
		}
	}

	// 服务费退款
	if param.DownRefundServiceAmount != 0 {
		_, err := beanpay.GetAccount(param.Ident, enum.AccountGroups.DownService).DrawingAmount(
			trans, param.DownChannelNo, param.TradeOrderNo, param.DownRefundServiceAmount, param.Memo)
		if err != nil {
			trans.Rollback()
			return err
		}
	}
	trans.Commit()
	return nil
}
