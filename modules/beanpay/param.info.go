package beanpay

// DownPay 下游扣款
type DownPay struct {
	Ident                string  `json:"ident" m2s:"ident" valid:"required"`                     //系统标识
	DownChannelNo        string  `json:"down_channel_no" m2s:"down_channel_no" valid:"required"` //下游渠道编号
	TradeOrderNo         string  `json:"trade_order_no" m2s:"trade_order_no" valid:"required"`   //外部系统交易订单号
	DownSellAmount       float64 `json:"down_sell_amount" m2s:"down_sell_amount"`                //销售金额
	DownCommissionAmount float64 `json:"down_commission_amount" m2s:"down_commission_amount" `   //佣金金额
	DownServiceAmount    float64 `json:"down_service_amount" m2s:"down_service_amount"`          //服务费金额
	DownFeeAmount        float64 `json:"down_fee_amount" m2s:"down_fee_amount"`                  //手续费金额
	Memo                 string  `json:"memo" m2s:"memo"`                                        //备注
}

// DownRefund 下游退款
type DownRefund struct {
	Ident                      string  `json:"ident" m2s:"ident" valid:"required"`                                //系统标识
	DownChannelNo              string  `json:"down_channel_no" m2s:"down_channel_no" valid:"required"`            //下游渠道编号
	TradeOrderNo               string  `json:"trade_order_no" m2s:"trade_order_no" valid:"required"`              //外部系统交易订单号
	TradeRefundNo              string  `json:"trade_refund_no" m2s:"trade_refund_no" valid:"required"`            //外部系统交易订单号
	DownRefundAmount           float64 `json:"down_refund_amount" m2s:"down_refund_amount"`                       //下游退款金额
	DownRefundCommissionAmount float64 `json:"down_refund_commission_amount" m2s:"down_refund_commission_amount"` //下游退款佣金
	DownRefundServiceAmount    float64 `json:"down_refund_service_amount" m2s:"down_refund_service_amount"`       //下游退款服务费
	DownRefundFeeAmount        float64 `json:"down_refund_fee_amount" m2s:"down_refund_fee_amount"`               //下游退款手续费
	Memo                       string  `json:"memo" m2s:"memo" `                                                  //备注
}

// UpPay 上游扣款
type UpPay struct {
	Ident              string  `json:"ident" m2s:"ident" valid:"required"`                         //系统标识
	UpChannelNo        string  `json:"up_channel_no" m2s:"up_channel_no" valid:"required"`         //上游渠道编号
	TradeDeliveryNo    string  `json:"trade_delivery_no" m2s:"trade_delivery_no" valid:"required"` //发货编号
	UpCostAmount       float64 `json:"up_cost_amount" m2s:"up_cost_amount"`                        //上游成本金额
	UpCommissionAmount float64 `json:"up_commission_amount" m2s:"up_commission_amount"`            //上游佣金金额
	UpServiceAmount    float64 `json:"up_service_amount" m2s:"up_service_amount" `                 //上游服务费金额
	Memo               string  `json:"memo" m2s:"memo"`                                            //备注
}

//UpRefund 上游退款
type UpRefund struct {
	Ident                    string  `json:"ident" m2s:"ident" valid:"required"`                            //系统标识
	UpChannelNo              string  `json:"up_channel_no" m2s:"up_channel_no" valid:"required"`            //上游渠道号
	TradeDeliveryNo          string  `json:"trade_delivery_no" m2s:"trade_delivery_no" valid:"required"`    //发货编号
	TradeRefundNo            string  `json:"trade_refund_no" m2s:"trade_refund_no" valid:"required"`        //退款编号
	UpRefundAmount           float64 `json:"up_refund_amount" m2s:"up_refund_amount" `                      //上游退款金额
	UpRefundCommissionAmount float64 `json:"up_refund_commission_amount" m2s:"up_refund_commission_amount"` //上游退款佣金
	UpRefundServiceAmount    float64 `json:"up_refund_service_amount" m2s:"up_refund_service_amount" `      //上游退款服务费
	Memo                     string  `json:"memo" m2s:"memo" `                                              //备注
}
