package beanpay

import (
	"gitee.com/micro-plat/fas/modules/const/enum"
	"gitee.com/micro-plat/fas/modules/sdkdb"
	"github.com/micro-plat/beanpay/beanpay"
	"github.com/micro-plat/lib4go/db"
	"github.com/micro-plat/lib4go/types"
)

// CreateAccount 创建账户
func CreateAccount(i interface{}, ident, accountType, eid, name string) error {
	// 获取数据操作对象
	m, trans, err := sdkdb.GetDBTrans(i)
	if err != nil {
		return err
	}
	err = createAccount(trans, ident, accountType, eid, name)
	if !m {
		return err
	}

	if err != nil {
		trans.Rollback()
		return err
	}

	trans.Commit()
	return nil
}

func createAccount(i db.IDBTrans, ident, accountType, eid, name string) error {
	_, err := beanpay.GetAccount(ident, types.DecodeString(accountType, enum.AccountTypes.Down,
		enum.AccountGroups.DownChannel, enum.AccountGroups.UpChannel)).CreateAccount(i, eid, name)
	if err != nil {
		return err
	}

	_, err = beanpay.GetAccount(ident, types.DecodeString(accountType, enum.AccountTypes.Down,
		enum.AccountGroups.DownCommission, enum.AccountGroups.UpCommission)).CreateAccount(i, eid, name)
	if err != nil {
		return err
	}

	_, err = beanpay.GetAccount(ident, types.DecodeString(accountType,
		enum.AccountTypes.Down, enum.AccountGroups.DownService, enum.AccountGroups.UpService)).CreateAccount(i, eid, name)

	return err
}
