package fas

import (
	"gitee.com/micro-plat/fas/modules/const/conf"
)

type Option func()

func WithSupSystemID(systemID string) Option {
	return func() {
		conf.SupSetting.SystemID = systemID
	}
}

func WithSupCompanyID(companyID string) Option {
	return func() {
		conf.SupSetting.CompanyID = companyID
	}
}

func WithDBName(db string) Option {
	return func() {
		conf.DBName = db
	}
}

func WithKeepAccount(keepAccount bool) Option {
	return func() {
		conf.KeepAccount = keepAccount
	}
}

// Config 配置
func Config(opts ...Option) {
	for _, opt := range opts {
		opt()
	}
}
