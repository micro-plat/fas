package fas

// DownPay 下游扣款
type DownPay struct {
	Ident                string  `json:"ident" m2s:"ident" valid:"required"`                                    //系统标识
	OrderSource          int     `json:"order_source" m2s:"order_source" valid:"required"`                      //来源系统编号
	DownChannelNo        string  `json:"down_channel_no" m2s:"down_channel_no" valid:"required"`                //下游渠道编号
	TradeOrderNo         string  `json:"trade_order_no" m2s:"trade_order_no" valid:"required"`                  //外部系统交易订单号
	ExtOrderNo           string  `json:"ext_order_no" m2s:"ext_order_no" valid:"required"`                      //下游商户的订单号
	RechargeAccountNo    string  `json:"recharge_account_no" m2s:"recharge_account_no" valid:"required"`        //充值账号
	BusinessType         int     `json:"business_type" m2s:"business_type" valid:"required"`                    //业务类型
	CarrierNo            string  `json:"carrier_no" m2s:"carrier_no" valid:"required"`                          //运营商
	ProvinceNo           string  `json:"province_no" m2s:"province_no" valid:"required"`                        //省份
	CityNo               string  `json:"city_no" m2s:"city_no" valid:"required"`                                //城市
	TotalFace            int     `json:"total_face" m2s:"total_face" valid:"range(0|999999999999999999)"`       //面值
	RechargeUnit         int     `json:"recharge_unit" m2s:"recharge_unit" valid:"range(0|999999999999999999)"` //充值量单位（多少M）
	OrderTime            string  `json:"order_time" m2s:"order_time" valid:"required"`                          //订单时间
	DownSellAmount       float64 `json:"down_sell_amount" m2s:"down_sell_amount" valid:"required"`              //销售金额
	DownCommissionAmount float64 `json:"down_commission_amount" m2s:"down_commission_amount" valid:"required"`  //佣金金额
	DownServiceAmount    float64 `json:"down_service_amount" m2s:"down_service_amount" valid:"required"`        //服务费金额
	DownFeeAmount        float64 `json:"down_fee_amount" m2s:"down_fee_amount" valid:"required"`                //手续费金额
	Memo                 string  `json:"memo" m2s:"memo" valid:"required"`                                      //备注
}

// DownPayResult 下游扣款结果
type DownPayResult struct {
	Result string `json:"result"`
	Msg    string `json:"msg"`
}

// SupDownPay 下游扣款
type SupDownPay struct {
	Ident                string  `json:"ident" m2s:"ident" valid:"required"`                                   //系统标识
	DownChannelNo        string  `json:"down_channel_no" m2s:"down_channel_no" valid:"required"`               //下游渠道编号
	TradeOrderNo         string  `json:"trade_order_no" m2s:"trade_order_no" valid:"required"`                 //下游交易订单号
	BusinessType         int     `json:"business_type" m2s:"business_type" valid:"required"`                   //业务类型，记账系统的
	CarrierNo            string  `json:"carrier_no" m2s:"carrier_no" valid:"required"`                         //运营商
	ProvinceNo           string  `json:"province_no" m2s:"province_no" valid:"required"`                       //省份
	CityNo               string  `json:"city_no" m2s:"city_no" valid:"required"`                               //城市
	OrderUnit            int     `json:"order_unit" m2s:"order_unit" valid:"range(0|99999999999999999999)"`    //订单规格
	OrderFace            int     `json:"order_face" m2s:"order_face" valid:"range(0|99999999999999999999)"`    //订单金额面值
	DownSellAmount       float64 `json:"down_sell_amount" m2s:"down_sell_amount" valid:"required"`             //销售金额
	DownCommissionAmount float64 `json:"down_commission_amount" m2s:"down_commission_amount" valid:"required"` //佣金金额
	DownServiceAmount    float64 `json:"down_service_amount" m2s:"down_service_amount" valid:"required"`       //服务费金额
	DownFeeAmount        float64 `json:"down_fee_amount" m2s:"down_fee_amount" valid:"required"`               //手续费金额
	OrderTime            string  `json:"order_time" m2s:"order_time" valid:"required"`                         //生成系统订单生成时间，格式：yyyy-mm-dd hh24:mi:ss
	ChangeTime           string  `json:"change_time" m2s:"change_time" valid:"required"`                       //生成系统下游扣款时间，格式：yyyy-mm-dd hh24:mi:ss
	Memo                 string  `json:"memo" m2s:"memo" valid:"required"`
}

// UpPay 上游扣款
type UpPay struct {
	Ident                string  `json:"ident" m2s:"ident" valid:"required"`                                        //系统标识
	OrderSource          int     `json:"order_source" m2s:"order_source" valid:"required"`                          //来源系统编号
	DownChannelNo        string  `json:"down_channel_no" m2s:"down_channel_no" valid:"required"`                    //下游渠道编号
	UpChannelNo          string  `json:"up_channel_no" m2s:"up_channel_no" valid:"required"`                        //上游渠道编号
	TradeOrderNo         string  `json:"trade_order_no" m2s:"trade_order_no" valid:"required"`                      //订单号
	TradeDeliveryNo      string  `json:"trade_delivery_no" m2s:"trade_delivery_no" valid:"required"`                //发货编号
	ExtOrderNo           string  `json:"ext_order_no" m2s:"ext_order_no" valid:"required"`                          //外部系统交易订单号
	RechargeAccountNo    string  `json:"recharge_account_no" m2s:"recharge_account_no" valid:"required"`            //充值编号
	BillType             int     `json:"bill_type" m2s:"bill_type" valid:"required"`                                //记账类型
	BusinessType         int     `json:"business_type" m2s:"business_type" valid:"required"`                        //业务类型
	CarrierNo            string  `json:"carrier_no" m2s:"carrier_no" valid:"required"`                              //运营商
	ProvinceNo           string  `json:"province_no" m2s:"province_no" valid:"required"`                            //省份
	CityNo               string  `json:"city_no" m2s:"city_no" valid:"required"`                                    //城市
	DownOrderUnit        int     `json:"down_order_unit" m2s:"down_order_unit" valid:"range(0|999999999999999999)"` //下游订单单价
	DownOrderFace        int     `json:"down_order_face" m2s:"down_order_face" valid:"range(0|999999999999999999)"` //下游订单面值
	DownDrawUnit         int     `json:"down_draw_unit" m2s:"down_draw_unit" valid:"range(0|99999999999999999999)"` //下游单价
	DownDrawFace         int     `json:"down_draw_face" m2s:"down_draw_face" valid:"range(0|99999999999999999999)"` //下游面值
	DownSellAmount       float64 `json:"down_sell_amount" m2s:"down_sell_amount" valid:"required"`                  //下游销售金额
	DownCommissionAmount float64 `json:"down_commission_amount" m2s:"down_commission_amount" valid:"required"`      //下游佣金金额
	DownServiceAmount    float64 `json:"down_service_amount" m2s:"down_service_amount" valid:"required"`            //下游服务费金额
	DownFeeAmount        float64 `json:"down_fee_amount" m2s:"down_fee_amount" valid:"required"`                    //下游手续费金额
	UpDrawUnit           int     `json:"up_draw_unit" m2s:"up_draw_unit" valid:"range(0|99999999999999999999)"`     //上游单价
	UpDrawFace           int     `json:"up_draw_face" m2s:"up_draw_face" valid:"range(0|99999999999999999999)"`     //上游面值
	UpCostAmount         float64 `json:"up_cost_amount" m2s:"up_cost_amount" valid:"required"`                      //上游成本金额
	UpCommissionAmount   float64 `json:"up_commission_amount" m2s:"up_commission_amount" valid:"required"`          //上游佣金金额
	UpServiceAmount      float64 `json:"up_service_amount" m2s:"up_service_amount" valid:"required"`                //上游服务费金额
	OrderTime            string  `json:"order_time" m2s:"order_time" valid:"required"`                              //订单时间
	Memo                 string  `json:"memo" m2s:"memo" valid:"required"`                                          //备注
}

// UpPayResult 上游扣款结果
type UpPayResult struct {
	Result string `json:"result"`
	Msg    string `json:"msg"`
}

// SupUpPay sup上游扣款
type SupUpPay struct {
	Ident              string  `json:"ident" m2s:"ident" valid:"required"`                                      //系统标识
	UpChannelNo        string  `json:"up_channel_no" m2s:"up_channel_no" valid:"required"`                      //上游渠道编号
	DownChannelNo      string  `json:"down_channel_no" m2s:"down_channel_no" valid:"required"`                  //下游渠道编号
	TradeOrderNo       string  `json:"trade_order_no" m2s:"trade_order_no" valid:"required"`                    //交易订单号
	TradeDeliveryNo    string  `json:"trade_delivery_no" m2s:"trade_delivery_no" valid:"required"`              //交易发货编号
	BillType           string  `json:"bill_type" m2s:"bill_type" valid:"in(1|2)"`                               //开票信息，前后项，1前向，不开票，2后项，需要开票
	BusinessType       int     `json:"business_type" m2s:"business_type" valid:"required"`                      //业务类型，记账系统的
	CarrierNo          string  `json:"carrier_no" m2s:"carrier_no" valid:"required"`                            //运营商
	ProvinceNo         string  `json:"province_no" m2s:"province_no" valid:"required"`                          //省份
	CityNo             string  `json:"city_no" m2s:"city_no" valid:"required"`                                  //城市编号
	OrderUnit          int     `json:"order_unit" m2s:"order_unit" valid:"range(0|99999999999999999999)"`       //订单规格
	OrderFace          int     `json:"order_face" m2s:"order_face" valid:"range(0|99999999999999999999)"`       //订单金额面值
	OrderAmount        float64 `json:"order_amount" m2s:"order_amount" valid:"range(0|99999999999999999999)"`   //订单金额
	UpCostAmount       float64 `json:"up_cost_amount" m2s:"up_cost_amount" valid:"required"`                    //上游成本金额
	UpCommissionAmount float64 `json:"up_commission_amount" m2s:"up_commission_amount"`                         //上游佣金金额
	UpServiceAmount    float64 `json:"up_service_amount" m2s:"up_service_amount"`                               //上游服务费金额
	UpDrawCount        int     `json:"up_draw_count" m2s:"up_draw_count" valid:"range(0|99999999999999999999)"` //上游扣款消耗库存卡张数
	UpDrawUnit         int     `json:"up_draw_unit" m2s:"up_draw_unit" valid:"range(0|99999999999999999999)"`   //上游扣款规格
	UpDrawFace         int     `json:"up_draw_face" m2s:"up_draw_face" valid:"range(0|99999999999999999999)"`   //上游扣款金额面值
	OrderTime          string  `json:"order_time" m2s:"order_time" valid:"required"`                            //订单创建时间
	ChangeTime         string  `json:"change_time" m2s:"change_time" valid:"required"`                          //上游扣款发生时间SupDownOrderPay
	Memo               string  `json:"memo" m2s:"memo" valid:"required"`                                        //备注信息
}

//DownRefund 下游退款
type DownRefund struct {
	Ident                      string  `json:"ident" m2s:"ident" valid:"required"`                                  //系统标识
	OrderSource                int     `json:"order_source" m2s:"order_source" valid:"required"`                    //来源系统编号
	DownChannelNo              string  `json:"down_channel_no" m2s:"down_channel_no" valid:"required"`              //下游渠道编号
	TradeOrderNo               string  `json:"trade_order_no" m2s:"trade_order_no" valid:"required"`                //外部系统交易订单号
	TradeRefundNo              string  `json:"trade_refund_no" m2s:"trade_refund_no" valid:"required"`              //外部系统交易订单号
	RefundUnit                 int     `json:"refund_unit" m2s:"refund_unit" valid:"range(0|99999999999999999999)"` //面值
	RefundFace                 int     `json:"refund_face" m2s:"refund_face" valid:"range(0|99999999999999999999)"` //面值
	DownRefundAmount           float64 `json:"down_refund_amount" m2s:"down_refund_amount" `                        //下游退款金额
	DownRefundCommissionAmount float64 `json:"down_refund_commission_amount" m2s:"down_refund_commission_amount"  ` //下游退款佣金
	DownRefundServiceAmount    float64 `json:"down_refund_service_amount" m2s:"down_refund_service_amount" `        //下游退款服务费
	DownRefundFeeAmount        float64 `json:"down_refund_fee_amount" m2s:"down_refund_fee_amount" `                //下游退款手续费
	OrderDate                  string  `json:"order_date" m2s:"order_date" valid:"required"`                        //订单数据
	Memo                       string  `json:"memo" m2s:"memo" valid:"required"`                                    //备注
}

//DownRefundResult 下游退款结果
type DownRefundResult struct {
	Result string `json:"result"`
	Msg    string `json:"msg"`
}

//SupDownRefund sup下游退款
type SupDownRefund struct {
	Ident                      string  `json:"ident" m2s:"ident" valid:"required"`                                                //系统标识
	DownChannelNo              string  `json:"down_channel_no" m2s:"down_channel_no" valid:"required"`                            //下游渠道编号
	TradeOrderNo               string  `json:"trade_order_no" m2s:"trade_order_no" valid:"required"`                              //下游交易订单号
	TradeRefundNo              string  `json:"trade_refund_no" m2s:"trade_refund_no" valid:"required"`                            //下游退款编号
	RefundFace                 int     `json:"refund_face" m2s:"refund_face" valid:"range(0|99999999999999999999)"`               //退款金额面值
	DownRefundAmount           float64 `json:"down_refund_amount" m2s:"down_refund_amount" valid:"range(0|99999999999999999999)"` //下游退款金额
	DownRefundCommissionAmount float64 `json:"down_refund_commission_amount" m2s:"down_refund_commission_amount"`                 //下游退款佣金
	DownRefundServiceAmount    float64 `json:"down_refund_service_amount" m2s:"down_refund_service_amount"`                       //下游退款服务费
	DownRefundFeeAmount        float64 `json:"down_refund_fee_amount" m2s:"down_refund_fee_amount" `                              //下游退款手续费
	OrderTime                  string  `json:"order_time" m2s:"order_time" valid:"required"`                                      //生成系统订单生成时间，格式：yyyy-mm-dd hh24:mi:ss
	ChangeTime                 string  `json:"change_time" m2s:"change_time" valid:"required"`                                    //生成系统下游退款时间，格式：yyyy-mm-dd hh24:mi:ss
	Memo                       string  `json:"memo" m2s:"memo" valid:"required"`                                                  //扣款备注信息
}

//UpRefund 上游退款
type UpRefund struct {
	Ident                    string  `json:"ident" m2s:"ident" valid:"required"`                                             //系统标识
	OrderSource              int     `json:"order_source" m2s:"order_source" valid:"required"`                               //来源系统编号
	DownChannelNo            string  `json:"down_channel_no" m2s:"down_channel_no" valid:"required"`                         //下游渠道号
	UpChannelNo              string  `json:"up_channel_no" m2s:"up_channel_no" valid:"required"`                             //上游渠道号
	TradeOrderNo             string  `json:"trade_order_no" m2s:"trade_order_no" valid:"required"`                           //订单编号
	TradeDeliveryNo          string  `json:"trade_delivery_no" m2s:"trade_delivery_no" valid:"required"`                     //发货编号
	TradeRefundNo            string  `json:"trade_refund_no" m2s:"trade_refund_no" valid:"required"`                         //退款编号
	BillType                 int     `json:"bill_type" m2s:"bill_type" valid:"required"`                                     //记账类型
	BusinessType             int     `json:"business_type" m2s:"business_type" valid:"required"`                             //业务类型
	CarrierNo                string  `json:"carrier_no" m2s:"carrier_no" valid:"required"`                                   //运营商
	ProvinceNo               string  `json:"province_no" m2s:"province_no" valid:"required"`                                 //省份
	UpRefundUnit             int     `json:"up_refund_unit" m2s:"up_refund_unit" valid:"range(0|99999999999999999999)"`      //退款unit
	UpRefundFace             int     `json:"up_refund_face" m2s:"up_refund_face" valid:"range(0|99999999999999999999)"`      //上游退款面值
	UpRefundAmount           float64 `json:"up_refund_amount" m2s:"up_refund_amount" valid:"range(0|99999999999999999999)"`  //上游退款金额
	UpRefundCommissionAmount float64 `json:"up_refund_commission_amount" m2s:"up_refund_commission_amount" valid:"required"` //上游退款佣金
	UpRefundServiceAmount    float64 `json:"up_refund_service_amount" m2s:"up_refund_service_amount" valid:"required"`       //上游退款服务费
	OrderTime                string  `json:"order_time" m2s:"order_time" valid:"required"`                                   //订单时间
	Memo                     string  `json:"memo" m2s:"memo" valid:"required"`                                               //备注
}

//UpRefundResult 上游退款结果
type UpRefundResult struct {
	Result string `json:"result"`
	Msg    string `json:"msg"`
}

//SupUpRefund sup上游退款
type SupUpRefund struct {
	Ident                    string  `json:"ident" m2s:"ident" valid:"required"`                                            //系统标识
	UpChannelNo              string  `json:"up_channel_no" m2s:"up_channel_no" valid:"required"`                            //上游渠道编号
	DownChannelNo            string  `json:"down_channel_no" m2s:"down_channel_no" valid:"required"`                        //下游渠道编号
	TradeOrderNo             string  `json:"trade_order_no" m2s:"trade_order_no" valid:"required"`                          //交易订单号
	TradeDeliveryNo          string  `json:"trade_delivery_no" m2s:"trade_delivery_no" valid:"required"`                    //订单发货编号
	TradeRefundNo            string  `json:"trade_refund_no" m2s:"trade_refund_no" valid:"required"`                        //订单上游退款编号
	BillType                 string  `json:"bill_type" m2s:"bill_type" valid:"range(1|2)"`                                  //开票信息，前后项，1前向，不开票，2后项，需要开票
	BusinessType             int     `json:"business_type" m2s:"business_type" valid:"required"`                            //业务类型，记账系统的
	UpRefundCount            int     `json:"up_refund_count" m2s:"up_refund_count" valid:"range(0|99999999999999999999)"`   //上游退款卡张数
	UpRefundUnit             int     `json:"up_refund_unit" m2s:"up_refund_unit" valid:"range(0|99999999999999999999)"`     //上游退款规格
	UpRefundFace             int     `json:"up_refund_face" m2s:"up_refund_face" valid:"range(0|99999999999999999999)"`     //上游退款金额面值
	UpRefundAmount           float64 `json:"up_refund_amount" m2s:"up_refund_amount" valid:"range(0|99999999999999999999)"` //上游退款金额
	UpRefundCommissionAmount float64 `json:"up_refund_commission_amount" m2s:"up_refund_commission_amount"`                 //上游退款佣金
	UpRefundServiceAmount    float64 `json:"up_refund_service_amount" m2s:"up_refund_service_amount" `                      //上游退款服务费
	UpRefundFeeAmount        float64 `json:"up_refund_fee_amount" m2s:"up_refund_fee_amount" `                              //上游退款手续费
	OrderTime                string  `json:"order_time" m2s:"order_time" valid:"required"`                                  //订单创建时间
	ChangeTime               string  `json:"change_time" m2s:"change_time" valid:"required"`                                //上游退款发生时间
	Memo                     string  `json:"memo" m2s:"memo" valid:"required"`                                              //备注信息
}

//ChannelList 渠道列表
type ChannelList struct {
	ChannelNo       string `json:"channel_no"`
	ChannelName     string `json:"channel_name"`
	CompanyID       string `json:"company_id"`
	Status          string `json:"status"`
	ChannelCompany  string `json:"channel_company"`
	ChannelFullName string `json:"channel_full_name"`
}

//DownChannelList 下游渠道列表
type DownChannelList struct {
	List []ChannelList `json:"result"`
}

//UpChannelList 上游渠道列表
type UpChannelList struct {
	List []ChannelList `json:"result"`
}

//ManualList 人工操作列表
type SupUpManualList struct {
	RecordID        string `json:"record_id"`
	ChangeType      string `json:"change_type"`
	WayChangeAmount string `json:"way_change_amount"`
	WayChangeFace   string `json:"way_change_face"`
	ChangeTime      string `json:"change_time"`
	CreateBy        string `json:"created_by"`
	AdjustRecordID  string `json:"adjust_record_id"`
	Memo            string `json:"memo"`
}

//ManualList 人工操作列表
type SupDownManualList struct {
	RecordID       string `json:"record_id"`
	ChangeType     string `json:"change_type"`
	ChangeAmount   string `json:"change_amount"`
	ChangeTime     string `json:"change_time"`
	CreateBy       string `json:"created_by"`
	AdjustRecordID string `json:"adjust_record_id"`
	Memo           string `json:"memo"`
}

//SupChannelSync sup渠道同步
type SupChannelSync struct {
	ChannelNo      string `json:"channel_no" m2s:"channel_no" valid:"required"`          //渠道编号
	ChannelName    string `json:"channel_name" m2s:"channel_name" valid:"required"`      //渠道名称
	CompanyID      string `json:"company_id" m2s:"company_id"`                           //渠道所属公司编号
	SystemID       string `json:"system_id" m2s:"system_id"`                             //渠道所属系统编号
	StatisticsType string `json:"statistics_type" m2s:"statistics_type" valid:"in(0|4)"` //上游渠道的财务分类，上游是4，下游是0
	ChannelType    string `json:"channel_type" m2s:"channel_type" valid:"in(1|2)"`       //渠道类型，1上游，2下游
	Status         string `json:"status" m2s:"status" valid:"in(0|1)"`                   //渠道状态，0启用，1禁用
	OperateUser    string `json:"operate_user" m2s:"operate_user" valid:"required"`      //操作人
}

//SupSyncResult 同步结果
type SupSyncResult struct {
	Result string `json:"result"`
	Msg    string `json:"msg"`
}

// QueryAccountListInfo 查询账户列表信息
type QueryAccountListInfo struct {
	Ident       string `json:"ident" m2s:"ident" valid:"required"`               // 系统标识
	PageIndex   int    `json:"page_index" m2s:"page_index" valid:"required"`     // 页码
	PageSize    int    `json:"page_size" m2s:"page_size" valid:"required"`       // 页数
	Group       string `json:"group" m2s:"group"`                                // 账户分组
	EID         string `json:"eid" m2s:"eid"`                                    // 外部用户账户编号
	AccountType string `json:"account_type" m2s:"account_type" valid:"required"` // 账户类型
	Name        string `json:"name" m2s:"name"`                                  // 账户名称
	Status      string `json:"status" m2s:"status"`                              // 账户状态
}

// QueryTradeRecordsInfo 查询交易记录信息
type QueryTradeRecordsInfo struct {
	Ident       string `json:"ident" m2s:"ident" valid:"required"` // 系统标识
	AccountType string `json:"account_type" m2s:"account_type" valid:"required"`
	AccountID   string `json:"account_id" m2s:"account_id"`
	Name        string `json:"name" m2s:"name"` // 账户名称
	ChangeType  string `json:"change_type" m2s:"change_type"`
	TradeType   string `json:"trade_type" m2s:"trade_type"`
	PageIndex   int    `json:"page_index" m2s:"page_index" valid:"required"` // 页码
	PageSize    int    `json:"page_size" m2s:"page_size" valid:"required"`   // 页数
	Group       string `json:"group" m2s:"group"`                            // 账户分组
	EID         string `json:"eid" m2s:"eid"`                                // 外部用户账户编号
	StartTime   string `json:"start_time" m2s:"start_time"`                  // 开始时间
	EndTime     string `json:"end_time" m2s:"end_time"`                      // 结束时间
}

//ZeroBalance 零点余额列表
type ZeroBalance struct {
	ChannelNo   string `json:"channel_no"`
	ZeroBalance string `json:"zero_balance"`
	ChannelType string `json:"channel_type"`
	ZeroDate    string `json:"zero_date"`
}
