//go:build !beanpay
// +build !beanpay

package fas

import (
	"encoding/json"
	"fmt"

	"gitee.com/micro-plat/fas/modules/const/enum"
	"gitee.com/micro-plat/fas/modules/inner/normal"
	"gitee.com/micro-plat/fas/modules/inner/sup"
	"github.com/micro-plat/beanpay/beanpay/account"
	"github.com/micro-plat/lib4go/types"
)

//GetDownChannelList 获取记账系统下游渠道列表
//SourceSystemID 系统来源编号
func GetDownChannelList(SourceSystemID int) (list *DownChannelList, listType string, err error) {

	billing, err := normal.NewBilling()
	if err != nil {
		return nil, "", err
	}

	data, err := billing.GetDownChannelList(SourceSystemID)
	if err != nil {
		return nil, "", err
	}

	list = &DownChannelList{}

	if err = json.Unmarshal([]byte(data), list); err != nil {
		return nil, "", fmt.Errorf("json.Unmarshal,err:%+v,data:%s", err, data)
	}

	return list, enum.InnerChannelList, err
}

//GetUpChannelList 获取记账系统上游渠道列表
//SourceSystemID 系统来源编号
func GetUpChannelList(SourceSystemID int) (list *UpChannelList, listType string, err error) {

	billing, err := normal.NewBilling()
	if err != nil {
		return nil, "", err
	}
	data, err := billing.GetUpChannelList(SourceSystemID)
	if err != nil {
		return nil, "", err
	}

	list = &UpChannelList{}

	if err = json.Unmarshal([]byte(data), list); err != nil {
		return nil, "", fmt.Errorf("json.Unmarshal,err:%+v,data:%s", err, data)
	}

	return list, enum.InnerChannelList, err
}

//GetSupUpManualRecord sup上游账户人工操作记录
//upChannelNo-->上游渠道号
//changeTime-->记录时间 yyyy-mm-dd hh24:mi:ss 范围：开始时间点为changeTime减去一天，结束时间点为changeTime
func GetSupUpManualRecord(upChannelNo, changeTime string) (r *[]SupUpManualList, err error) {

	billing, err := sup.NewBilling()
	if err != nil {
		return nil, err
	}
	data, err := billing.UpManualRecord(upChannelNo, changeTime)
	if err != nil {
		return nil, err
	}

	r = &[]SupUpManualList{}

	if err = json.Unmarshal([]byte(data), r); err != nil {
		return nil, fmt.Errorf("json.Unmarshal,err:%+v,data:%s", err, data)
	}

	return
}

//GetSupDownManualRecord sup下游账户人工操作记录
//upChannelNo-->上游渠道号
//changeTime-->记录时间 yyyy-mm-dd hh24:mi:ss 范围：开始时间点为changeTime减去一天，结束时间点为changeTime
func GetSupDownManualRecord(downChannelNo, changeTime string) (r *[]SupDownManualList, err error) {

	billing, err := sup.NewBilling()
	if err != nil {
		return nil, err
	}
	data, err := billing.DownManualRecord(downChannelNo, changeTime)
	if err != nil {
		return nil, err
	}

	r = &[]SupDownManualList{}

	if err = json.Unmarshal([]byte(data), r); err != nil {
		return nil, fmt.Errorf("json.Unmarshal,err:%+v,data:%s", err, data)
	}

	return
}

//SupChannelInfoSync sup渠道同步
func SupChannelInfoSync(info *SupChannelSync) (r *SupSyncResult, err error) {

	param := &sup.ChannelInfoSync{
		ChannelNo:      info.ChannelNo,
		ChannelName:    info.ChannelName,
		CompanyID:      info.CompanyID,
		SystemID:       info.SystemID,
		StatisticsType: info.StatisticsType,
		ChannelType:    info.ChannelType,
		Status:         info.Status,
		OperateUser:    info.OperateUser,
	}

	billing, err := sup.NewBilling()
	if err != nil {
		return nil, err
	}

	result, err := billing.ChannelSync(param)
	if err != nil {
		return nil, err
	}

	r = &SupSyncResult{}

	err = types.Map2Struct(r, result, "json")
	return
}

// CreateAccount 创建账户
//i-->nil/db.IDBTrans/db.IDB
//ident-->系统标识,accountType-->账户类型,1下游渠道账户,2上游渠道账户,默认是上游渠道账户
// eid-->外部用户账户编号,name-->账户名称
func CreateAccount(i interface{}, ident, accountType, eid, name string) error {
	return nil
}

// UpdateAccount 修改账户
//i-->nil/db.IDBTrans/db.IDB
//ident-->系统标识,group-->用户分组,eid-->外部用户账户编号,name-->账户名称
func UpdateAccount(i interface{}, ident, group, eid, name string) (*account.AccountResult, error) {
	return nil, fmt.Errorf("记账系统不支持修改账户")
}

// GetAccount 查询账户信息
//i-->nil/db.IDBTrans/db.IDB
//ident-->系统标识,group-->用户分组,eid-->外部用户账户编号
func GetAccount(i interface{}, ident, group, eid string) (*account.Account, error) {
	return nil, fmt.Errorf("记账系统不支持查询账户信息")
}

// QueryAccount 查询账户信息列表
//i-->nil/db.IDBTrans/db.IDB
//ident-->系统标识,group-->用户分组,eid-->外部用户账户编号,name-->账户名称
// accountType-->账户类型,1下游渠道账户,2上游渠道账户,默认是上游渠道账户,pi-->第几页, ps-->每页多少条, status-->账户状态
func QueryAccount(i interface{}, input *QueryAccountListInfo) (r *account.AccountInfoList, err error) {
	return nil, fmt.Errorf("记账系统不支持查询账户信息列表")
}

// QueryTradeRecords 查询交易记录
//i-->nil/db.IDBTrans/db.IDB
//ident-->系统标识,group-->用户分组,eid-->外部用户账户编号,name-->账户名称
// startTime-->开始时间, endTime-->结束时间,pi-->第几页, ps-->每页多少条
func QueryTradeRecords(i interface{}, input *QueryTradeRecordsInfo) (*account.RecordResults, error) {
	return nil, fmt.Errorf("记账系统不支持查询交易记录")
}
